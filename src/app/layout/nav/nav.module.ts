import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav.component';
import { NgxBootstrapIconsModule, allIcons } from 'ngx-bootstrap-icons';
import { NoteclassComponent } from 'src/app/noteclass/noteclass.component';


@NgModule({
  declarations: [NavComponent],
  imports: [
    CommonModule,
    NgxBootstrapIconsModule.pick(allIcons)
  ],
  exports: [ NavModule ],
  entryComponents: [
    NoteclassComponent
  ]
})
export class NavModule { }
