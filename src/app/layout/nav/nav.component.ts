import { AuthService } from './../../auth/service/auth.service';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ViewCompileResult } from '@angular/compiler/src/view_compiler/view_compiler';
import { NoteclassComponent } from 'src/app/noteclass/noteclass.component';
import { TicketComponent } from 'src/app/ticket/ticket.component'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  [x: string]: any;
  public visibleNote: string;
  public visibleTask: string;
  public myComponentDinamic;
  constructor(public authSvc: AuthService) {
    this.myComponentDinamic = NoteclassComponent;
  }

  ngOnInit(): void {
    this.visibleNote = 'Si';
    this.visibleTask = 'No';
    let myComponent = NoteclassComponent;
    //this.authSvc.isTeacher()
   
  }
  public statusNote(): boolean{
    if ( this.visibleNote === 'Si'){
      this.visibleNote = 'No';
      return true;
    }
    else{
      this.visibleNote = 'Si';
      return false;
    }
  }
  public getComponent(comp: any){
    switch(comp){
      case 'Noteclass':
        this.myComponentDinamic = NoteclassComponent;
        break;
      case 'ticket':
        this.myComponentDinamic = TicketComponent;
        break;
    }
    
  }
  public statusTask(): boolean{
    if ( this.visibleTask === 'No'){
      this.visibleTask = 'Si';
      return true;
    }
    else{
      this.visibleTask = 'No';
      return false;
    }
  }
  public isTeacherW(){
    return this.authSvc.isTeacher();
  }
  
}
