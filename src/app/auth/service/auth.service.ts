import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { auth } from 'firebase/app';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User;
  userData: User; // Save logged in user data
  public statusUser = 'No';
  btnTeacher= false;
  public dataArray = [];

  constructor(public afs: AngularFirestore, public afAuth: AngularFireAuth, public router: Router, public ngZone: NgZone) {
    this.afAuth.authState.subscribe(user => {
      if ( user ){
        user = user;
        this.userData = user;
        this.statusUser = 'Si';
        localStorage.setItem('user', JSON.stringify(user));
        this.router.navigate(['dashboard']);
      }
      else{
        localStorage.setItem('user', null);
        this.router.navigate(['login']);
      }
    });
    
  }
// Sign in with email/password
SignIn(email, password) {
  return this.afAuth.signInWithEmailAndPassword(email, password)
    .then((result) => {
      this.ngZone.run(() => {
        this.router.navigate(['dashboard']);
      });
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error.message)
    })
}
// Sign up with email/password
SignUp(email, password) {
  return this.afAuth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error.message)
    })
}

 googleAuth(){
    return this.autoLogin(new auth.GoogleAuthProvider());
  }
  autoLogin(provider){
    return this.afAuth.signInWithPopup(provider)
    .then((result) =>{
      console.log('Inicio Correcto');
      this.SetUserData(result.user);
    }).catch((error) => {
      console.log('No se pudo');
    });
  }
  signOut(){
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.statusUser = "No";
    });
  }
  getUserStatus(){
    return localStorage.getItem('user');
  }
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      teacher : "no"
    }
    return userRef.set(userData, {
      merge: true
    });
  }
  iamTeacher(){
    const user =this.getUserStatus();
    const dataUser = JSON.parse(user);
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${dataUser.uid}`);
    userRef.update({
      "teacher": "yes"
    });
  }
  isTeacher(){
    return this.btnTeacher;   
  }
  allTeachers(){
    this.dataArray = [];
    /*var d = this.afs.collection('users').ref.where('teacher', '==', 'yes').onSnapshot(function(ls){
      var datos = [];
      var source = ls.metadata.hasPendingWrites ? "Local" : "Server";
      ls.forEach(function(doc){
        datos.push(doc.data());
      });
      return datos;
    });
    console.log(d);
  }*/
   this.afs.collection('users', ref => ref.where('teacher', '==', 'yes')).get().subscribe( ls =>{
    let dataDocs = [];
    ls.docs.forEach( doc =>{
      dataDocs.push(doc.data());
      this.dataArray.push(doc.data());
    });
    this.dataArray = dataDocs;
  });
  return this.dataArray;
  
}
}
