import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterComponent } from './register.component';
import { ReactiveFormsModule, FormControl, FormGroup, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormControl,
    FormGroup,
    FormsModule,
    RegisterComponent,
  ]
})
export class RegisterModule { }
