import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteclassComponent } from './noteclass.component';

describe('NoteclassComponent', () => {
  let component: NoteclassComponent;
  let fixture: ComponentFixture<NoteclassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoteclassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
