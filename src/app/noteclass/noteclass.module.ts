import { HttpService } from './../service/http.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteclassComponent } from './noteclass.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [NoteclassComponent],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
  ],
  providers: [HttpService],
  exports: [NoteclassModule]
})
export class NoteclassModule { }
