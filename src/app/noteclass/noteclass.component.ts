import { AuthService } from './../auth/service/auth.service';
import { FireService } from './../service/fire.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-noteclass',
  templateUrl: './noteclass.component.html',
  styleUrls: ['./noteclass.component.css']
})
export class NoteclassComponent implements OnInit {

  formNoteClass: FormGroup;
  public notes;

  constructor(private fb: FormBuilder, private AuhtSvc: AuthService, private fire: FireService, private alertService: AlertService) {
    const uidx = JSON.parse(this.AuhtSvc.getUserStatus());
    this.fire.getNote(uidx.uid);
  }

  ngOnInit(): void {
    this.formNoteClass = this.fb.group({
      nameclass: new FormControl(),
      note: new FormControl(),
    });
    const uidx = JSON.parse(this.AuhtSvc.getUserStatus());
    this.fire.getNote(uidx.uid).subscribe((data) => {
          this.notes = [];
          data.forEach((inData)=>{
            this.notes.push({
              id: inData.payload.doc.id,
              data: inData.payload.doc.data(),
            })
          })
    });
  }

  public onSaveNote() {
    const { nameclass, note } = this.formNoteClass.value;
    const uidx = JSON.parse(this.AuhtSvc.getUserStatus());
    this.fire.saveNote(uidx.uid, nameclass, note);
    this.formNoteClass.setValue({
      nameclass: '',
      note: '',
    });
    this.alertService.success('Nota Guardado');
  }
  public deleteNote(id: string){
    const uidx = JSON.parse(this.AuhtSvc.getUserStatus());
    this.fire.delNote(uidx.uid, id);
    this.alertService.danger('Elemento Eliminado');
  }

}
