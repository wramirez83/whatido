import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { NavModule } from './../layout/nav/nav.module';

@NgModule({
  declarations: [ DashboardComponent],
  imports: [
    CommonModule,
  ]
})
export class DashboardModule { }
