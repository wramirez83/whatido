import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { NoteclassComponent } from '../noteclass/noteclass.component';


@Injectable({
  providedIn: 'root'
})
export class FireService {

  constructor(public afs: AngularFirestore) { }
  notesData: AngularFirestoreDocument<any>;
  saveNote(uid, nameclass, note): void {
    //const addNote: AngularFirestoreDocument<any> = this.afs.doc(`note/${uid}`);
    const addNote = this.afs.collection('data').doc(uid).collection('notes');
    const dataNote = {
      nameclass,
      note,
      date: new Date(),
      status: 'Activo'
    };
    addNote.add(dataNote);
    console.log(uid);
  }


  getNote(uid: string) {
    const getData = this.afs.collection('data').doc(uid).collection('notes').snapshotChanges();
    return getData;
  }
  delNote(uid: string, id: string){
    const a = this.afs.collection('data').doc(uid).collection('notes').doc(id).delete();
    console.log(`data/${uid}/notes/${id}`);
  }
  onSaveTicket(uid, teacher, typeRequest, topic, commentary){
    const addTicket = this.afs.collection('data').doc(uid).collection('ticket');
    const dataTicket = {
      uid, 
      teacher, 
      typeRequest, 
      topic, 
      commentary,
      date: new Date(),
      status: 'Activo'
    }
    addTicket.add(dataTicket);
  }
  getTicketsUser(uid: string){
    const getData = this.afs.collection('data').doc(uid).collection('ticket').snapshotChanges();
    return getData;
  }
  onCloseTicket(uid, id, status){
    console.log(`data/${uid}/ticket/${id}`)
    const updateData = this.afs.doc(`data/${uid}/ticket/${id}`);
    updateData.update({
      status: status
    });
  }
  onDeleteTicket(uid, id){
    //const deleteData = this.afs.doc(`data/${uid}/ticket/${id}`);
    this.afs.collection('data').doc(uid).collection('ticket').doc(id).delete();
  }

}
