import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketComponent } from './ticket.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [TicketComponent],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
  ]
})
export class TicketModule { }
