import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import {FormBuilder, FormGroup, FormControl} from '@angular/forms'
import { FireService } from '../service/fire.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {
  public teachers;
  public tickets: any[];
  public formTicket: FormGroup;
  public typeRequests = [
    {
      id: 0,
      type: 'Asesoria'
    },
    {
      id: 1,
      type: 'Asistencia'
    },
    {
      id: 2,
      type: 'Otro'
    }
  ];
  constructor(private authSvc: AuthService, private fire: FireService) {
    this.formTicket = new FormGroup(
      {
        teacher: new FormControl(''),
        typeRequest: new FormControl(''),
        topic: new FormControl(''),
        commentary: new FormControl(''),
      }
    );
    const uidx = JSON.parse(this.authSvc.getUserStatus());
    this.fire.getTicketsUser(uidx.uid).subscribe((data) => {
      this.tickets = [];
      data.forEach((inData) => {
        this.tickets.push({
          idT : inData.payload.doc.id,
           data: inData.payload.doc.data()
        });
      })
    })
  }

  ngOnInit(): void {
   this.teachers = this.authSvc.allTeachers();
  }
  onSaveTicket(){
    const { teacher, typeRequest, topic, commentary } = this.formTicket.value;
    const uidx = JSON.parse(this.authSvc.getUserStatus());
    this.fire.onSaveTicket(uidx.uid, teacher, typeRequest, topic, commentary);
    this.formTicket.setValue(
      {
        teacher: '',
        typeRequest:'',
        topic:'',
        commentary:'',
      }
    )
  }
  onCloseTicket(id: string, status: string){
    const uidx = JSON.parse(this.authSvc.getUserStatus());
    this.fire.onCloseTicket(uidx.uid, id, status);
  }
  onDeleteTicket(id){
    const uidx = JSON.parse(this.authSvc.getUserStatus());
    this.fire.onDeleteTicket(uidx.uid, id);
  }

}
